package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// 正規表現
	private static final String BRANCH_CODE_MATCHES = "^[0-9]{3}$";
	private static final String COMMODITY_CODE_MATCHES = "^[a-zA-Z0-9]{8}$";

	// ファイル名
	private static final String FILE_NAME_BRANCH = "支店定義ファイル";
	private static final String FILE_NAME_COMMODITY = "商品定義ファイル";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "が存在しません";
	private static final String SALES_FILE_NOT_SERIAL_NUMBER  = "売上ファイル名が連番になっていません";
	private static final String TOTAL_AMOUNT_DIGIT_OVER  = "合計金額が10桁を超えました";
	private static final String BRANCH_CODE_INVALID  = "の支店コードが不正です";
	private static final String INVALID_FOEMAT = "のフォーマットが不正です";
	private static final String SALES_CODE_INVALID = "の商品コードが不正です";


	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// コマンドライン引数が設定されているか
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
		}
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String,String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String,Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales,BRANCH_CODE_MATCHES,FILE_NAME_BRANCH)) {
			return;
		}
		// 商品定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales,COMMODITY_CODE_MATCHES,FILE_NAME_COMMODITY)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();

		for (int i = 0; i < files.length; i++) {
			if((files[i].isFile()) && (files[i].getName().matches("^[0-9]{8}.rcd$"))) {
				rcdFiles.add(files[i]);
			}
		}
		// ソートの処理
		Collections.sort(rcdFiles);
		// 売上ファイルが連番かどうか（エラー処理2-1）
		for(int i = 0; i < rcdFiles.size() - 1; i++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0,8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0,8));

			if((latter - former) != 1) {
				System.out.println(SALES_FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}

		// 売上ファイルを読み込む
		BufferedReader br = null;
		for (int i = 0; i < rcdFiles.size(); i++) {
			try {
				File file = rcdFiles.get(i);
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);

				String line = "";

				List<String> branchItems = new ArrayList<>();

				while ((line = br.readLine()) != null) {
					// 売上ファイルの支店コードと商品コードと売上額を保持するリストを作成
					branchItems.add(line);
				}

				// 支店コード
				String branchCode = branchItems.get(0);
				// 商品コード
				String commodityCode = branchItems.get(1);
				// 売上金額
				String subTotal = branchItems.get(2);

				// 売上ファイルの商品コード確認
				if(!commodityNames.containsKey(commodityCode)) {
					System.out.println(commodityCode + SALES_CODE_INVALID);
					return;
				}

				// 支店に該当がなかった場合
				if(!branchNames.containsKey(branchCode)) {
					System.out.println(branchCode + BRANCH_CODE_INVALID);
					return;
				}

				// 売上ファイルのフォーマットの確認（エラー処理2-2）
				if(branchItems.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + INVALID_FOEMAT);
					return;
				}
				if(!subTotal.matches("^[0-9]+$")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}

				long fileSales = Long.parseLong(subTotal);
				long salesAmount = branchSales.get(branchCode) + fileSales;
				// 商品定義ファイルの売上金額加算
				long commoditysAmount = commoditySales.get(commodityCode) + fileSales;

				//10桁を超えた場合の処理（エラー処理2-2）
				if ((salesAmount >= 10000000000L) || (commoditysAmount >= 10000000000L)) {
					System.out.println(TOTAL_AMOUNT_DIGIT_OVER);
					return;
				}

				branchSales.put(branchCode,salesAmount);
				// commoditySalesへ追加
				commoditySales.put(commodityCode, commoditysAmount);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				//ファイルを開いている場合
				if (br != null) {
					try {
						//ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}

	/**
	 * 支店定義ファイル読み込み処理
	 * 商品定義ファイル読み込み処理
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名、商品コードと商品名を保持するMap
	 * @param 支店コードと売上金額、商品コードと売上金額を保持するMap
	 * @param 正規表現式
	 * @param エラーメッセージ
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> NamesMap,
			Map<String, Long> SalesMap, String matches, String errorMessage) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			//ファイルが存在しない場合
			if(!file.exists()) {
				System.out.println(errorMessage + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {

				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] items = line.split(",");

				//フォーマットが不正の場合（エラー処理）
				if(items.length != 2 || !items[0].matches(matches)) {
					System.out.println(errorMessage + INVALID_FOEMAT);
					return false;
				}
				NamesMap.put(items[0], items[1]);
				SalesMap.put(items[0], (long) 0);

			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 * 商品別集計ファイル書き込み処理
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名、商品コードと商品名を保持するMap
	 * @param 支店コードと売上金額、商品コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */

	private static boolean writeFile(String path, String fileName, Map<String, String> namesMap,
			Map<String, Long> salesMap) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			 File file = new File(path,fileName);
			 FileWriter fw = new FileWriter(file);
			 bw = new BufferedWriter(fw);

			 for(String key : namesMap.keySet()) {
				 bw.write(key + "," + namesMap.get(key) + "," +  salesMap.get(key));
				 bw.newLine();
			 }

		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			//書き込みできなかった場合
			if(bw != null) {
				try {
					//ファイルを閉じる
					bw.close();
				}catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
